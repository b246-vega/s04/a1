from abc import ABC, abstractmethod

class Animal(ABC):
    @abstractmethod
    def eat(self, food):
        pass
    
    @abstractmethod
    def make_sound(self):
        pass
    
class Cat(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age
    
    # Getters
    def get_name(self):
        return self._name
    
    def get_breed(self):
        return self._breed
    
    def get_age(self):
        return self._age
    
    # Setters
    def set_name(self, name):
        self._name = name
    
    def set_breed(self, breed):
        self._breed = breed
    
    def set_age(self, age):
        self._age = age
    
    def eat(self, food):
        print(f"{self._name} is eating {food}.")
    
    def make_sound(self):
        print(f"{self._name} says 'Meow!'.")
    
    def call(self):
        print(f"{self._name} comes running.")
        
class Dog(Animal):
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age
    
# Getters
    def get_name(self):
        return self._name
    
    def get_breed(self):
        return self._breed
    
    def get_age(self):
        return self._age
    
# Setters
    def set_name(self, name):
        self._name = name
    
    def set_breed(self, breed):
        self._breed = breed
    
    def set_age(self, age):
        self._age = age
    
    def eat(self, food):
        print(f"{self._name} is eating {food}.")
    
    def make_sound(self):
        print(f"{self._name} says 'Woof!'.")
    
    def call(self):
        print(f"{self._name} comes running.")
